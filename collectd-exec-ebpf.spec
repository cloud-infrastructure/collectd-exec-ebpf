%global debug_package %{nil}

Name:           collectd-exec-ebpf
Version:        0.2
Release:        1%{?dist}
Summary:        Simple collectd exec plugin to retrieve eBPF metrics
License:        GPLv3
Group:          Development/Libraries
Url:            https://gitlab.cern.ch/cloud-infrastructure/collectd-exec-ebpf

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd

BuildRequires:  gcc
BuildRequires:  libbpf-devel

%{?systemd_requires}
%if 0%{?el8}
BuildRequires: systemd
%else
BuildRequires: systemd-rpm-macros
BuildRequires: clang
BuildRequires: llvm
%endif


%description
Simple collectd exec plugin that retrieves metrics via eBPF to be used by
the CERN Cloud Infrastructure service to help in their operations.


%prep
%setup -q -n %{name}-%{version}


%build
gcc -o cci-ebpf-collector cci-ebpf-collector.c  -lbpf -lelf

%if 0%{?el8}
# We don't build on el8 because we need to do it from the kernel.
# We ship the ELF object ebpf_mmu_notifier_seq.o
%else
# EL9 and newer we benefit from /sys/kernel/btf/vmlinux generation and
# CO-RE operations, so we can compile on the fly like this:
%ifarch x86_64
%define target_arch __TARGET_ARCH_x86
%endif
%ifarch aarch64
%define target_arch __TARGET_ARCH_arm64
%endif
mv CO-RE/vmlinux_%{_arch}.h CO-RE/vmlinux.h
clang -S \
    -target bpf \
    -D __BPF_TRACING__ \
    -D %{target_arch} \
    -Wall \
    -Werror \
    -O2 -emit-llvm -c -g CO-RE/ebpf_mmu_notifier_seq.c
llc -march=bpf -filetype=obj -o CO-RE/ebpf_mmu_notifier_seq.o ebpf_mmu_notifier_seq.ll

%endif


%install
install -p -D -m 0755 cci-ebpf-collector         -t %{buildroot}%{_sbindir}/
install -p -D -m 0755 collectd-exec-ebpf.sh      -t %{buildroot}%{_bindir}/
install -p -D -m 0644 cci-ebpf-collector.service -t %{buildroot}%{_unitdir}/
install -p -D -m 0644 cci-ebpf-collector.timer   -t %{buildroot}%{_unitdir}/

%if 0%{?el8}
install -p -D -m 0644 non-CO-RE/ebpf_mmu_notifier_seq.o -t %{buildroot}%{_libexecdir}/cci/
%else
install -p -D -m 0644 CO-RE/ebpf_mmu_notifier_seq.o     -t %{buildroot}%{_libexecdir}/cci/
%endif

%post
%systemd_post collectd.service cci-ebpf-collector.service cci-ebpf-collector.timer


%preun
%systemd_preun collectd.service cci-ebpf-collector.service cci-ebpf-collector.timer


%postun
%systemd_postun collectd.service cci-ebpf-collector.service cci-ebpf-collector.timer


%files
%defattr(-,root,root,-)
%{_bindir}/collectd-exec-ebpf.sh
%{_sbindir}/cci-ebpf-collector
%{_unitdir}/cci-ebpf-collector.service
%{_unitdir}/cci-ebpf-collector.timer
%{_libexecdir}/cci/ebpf_mmu_notifier_seq.o

%changelog
* Tue Dec 18 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.2-1
- Add more robust PID query for UUID to avoid multiple matches
* Tue Nov 21 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.1-1
- First version collectd_ebpf to collect mmu_notifier_seq metric
