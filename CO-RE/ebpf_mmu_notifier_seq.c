#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_tracing.h>

char _license[] SEC("license") = "GPL";

struct {
  __uint(type, BPF_MAP_TYPE_HASH);
  __type(key, long);
  __type(value, unsigned long);
  __uint(max_entries, 128);
} faults_map SEC(".maps");


SEC("kprobe/direct_page_fault")
int BPF_KPROBE(direct_page_fault, struct kvm_vcpu *vcpu)
{
  long unsigned int seq;
  BPF_CORE_READ_INTO(&seq, vcpu, kvm, mmu_invalidate_seq);

  long pid = bpf_get_current_pid_tgid() >> 32;

  bpf_map_update_elem(&faults_map, &pid, &seq, BPF_ANY);

  return 0;
}
