#if defined(CONFIG_FUNCTION_TRACER)
#define CC_USING_FENTRY
#endif

#include <linux/kvm_host.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>

char _license[] SEC("license") = "GPL";

struct {
  __uint(type, BPF_MAP_TYPE_HASH);
  __type(key, long);
  __type(value, unsigned long);
  __uint(max_entries, 128);
} faults_map SEC(".maps");


SEC("kprobe/direct_page_fault")
int BPF_KPROBE(direct_page_fault, struct kvm_vcpu *vcpu)
{
  struct kvm *kvm_info;
  bpf_probe_read_kernel(&kvm_info, sizeof(kvm_info), &vcpu->kvm);

  unsigned long seq;
  bpf_probe_read_kernel(&seq, sizeof(seq), &kvm_info->mmu_notifier_seq);

  long pid = bpf_get_current_pid_tgid() >> 32;

  bpf_map_update_elem(&faults_map, &pid, &seq, BPF_ANY);

  return 0;
}
