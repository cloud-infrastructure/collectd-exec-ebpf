# collectd-exec-ebpf

Collectd exec package to extract eBPF metrics out of the Kernel
for Cloud operations.

As eBPF injection is a privileged operation, root privileges are required. Howerver,
`collectd-exec` doesn't allow to run as root. To adapt to this situation, we adopted
the following approach:

* eBPF metrics are collected and placed in `/tmp` locations.
* The script called by collectd fetches the information from those temporary files and
  it enriches them with all the required metadata to produce a valid `PUTVAL` command.

## Metrics

### `mmu_notifier_seq`

Gathered to help in mitigating [OS-16919](https://its.cern.ch/jira/browse/OS-16919).

#### EL8

The current version was compiled for RHEL8 under the kernel `4.18.0-477.21.1.el8_8.x86_64`:

* In RHEL8 we are not able to compile outside the kernel tree due to our dependency on `linux/kvm_host.h`.
* The result object is kernel dependant, but given the stability of the entities used, we shouldn't
  expect many problems in recent versions. However, this should be closely monitoring to make sure the
  numbers produced match the desired field.

#### Non EL8

Starting with EL9, the compilation can be done easily as we benefit from the vmlinux.h generation
mechanism to have access to the internal structures. Additionally, the implementation benefits as well
from the availability of the CO-RE operations that makes the eBPF program more portable across kernel
versions.

For the particular case of `mmu_notifier_seq`, the variable has been changed name in EL9 to `mmm_invaliate_seq`.
However, the metric is kept with the same name for the sake of simplicity while troubleshooting this
issue.

## Tips

### Observe the values

The logic of this collector is to populate a map from kernel space to user space with eBPF. This map
can be easily _watched_ with the following command:

```
watch -n1 bpftool map dump name faults_map

[{
        "key": 1940003,
        "value": 227248
    },{
        "key": 131017,
        "value": 46819
    }
]

```
