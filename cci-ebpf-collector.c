/*
 * This file is based on the ebpf-kill-example distribution
 * (https://github.com/niclashedam/ebpf-kill-example). Copyright (c) 2021 Niclas
 * Hedam.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <libgen.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
  char path[] = "/usr/libexec/cci/ebpf_mmu_notifier_seq.o";

  struct bpf_object *obj;
  struct bpf_link *link = NULL;

  int err = -1;

  // Open the eBPF object file with the path above
  obj = bpf_object__open_file(path, NULL);

  // Check for errors in opening the object file
  if (libbpf_get_error(obj)) {
    fprintf(stderr, "ERROR: opening BPF object file failed\n");
    return err;
  }

  // Find the program with the name "direct_page_fault" in the object file
  // This is the name of the function implemented in /usr/libexec/cci/mmu_kern.o
  struct bpf_program *prog =
      bpf_object__find_program_by_name(obj, "direct_page_fault");

  // Check for errors in finding the program
  if (!prog) {
    fprintf(stderr, "ERROR: program not found in object\n");
    goto cleanup;
  }

  // Load the eBPF object file into the kernel
  if (bpf_object__load(obj)) {
    fprintf(stderr, "ERROR: loading BPF object file failed\n");
    goto cleanup;
  }

  // Attach the program to the tracepoint
  link = bpf_program__attach(prog);
  if (libbpf_get_error(link)) {
    fprintf(stderr, "ERROR: bpf_program__attach failed\n");
    link = NULL;
    goto cleanup;
  }

  struct bpf_map *faults_map = bpf_object__find_map_by_name(obj, "faults_map");
  int faults_map_fd = bpf_map__fd(faults_map);
  long key = -1, prev_key;

  char *filename = "/tmp/cloud_ebpf.out";
  FILE *fp = fopen(filename, "w");
  if (fp == NULL)
  {
    printf("Error opening the file %s", filename);
    goto cleanup;
  }

  // We collectd metrics for some time. It's not always immediate.
  sleep(30);

  // We iterate through the known "faults_map" map
  // The keys are the PID and the value the current "mmu_notifier_seq"
  // counter.
  while (bpf_map_get_next_key(faults_map_fd, &prev_key, &key) == 0) {
    unsigned long value;
    int ret;
    ret = bpf_map_lookup_elem(faults_map_fd, &key, &value);
    if (ret) {
      fprintf(stderr, "Failed reading mmu count\n");
      goto cleanup;
    }
    fprintf(fp, "%ld %lu\n", key, value);
    prev_key = key;
  }

  err = 0;

cleanup:
  bpf_link__destroy(link);
  bpf_object__close(obj);
  fclose(fp);

  return err;
}
