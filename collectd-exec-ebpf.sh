#!/bin/bash

PARAM_INTERVAL=$1
INTERVAL="${PARAM_INTERVAL:-${COLLECTD_INTERVAL:-60}}"
FILE=/tmp/cloud_ebpf.out

while : ; do
  if [ -f "$FILE" ]; then
    while read p; do
      entry=($p)
      counter=${entry[1]}
      uuid=$(ps -q ${entry[0]} axu |grep -P "(?<=uuid=)[0-9a-f-]+" -o)
      if [ -z "$uuid" ]; then
        # UUID not found, maybe machine is gone?
        continue
      fi
      echo PUTVAL \"$(hostname)/cloud_ebpf-${uuid}/gauge-mmu_notifier_seq\" interval=$INTERVAL N:$counter
    done < /tmp/cloud_ebpf.out
  fi
  sleep "$INTERVAL"
done
